﻿using MyConsole.Classes;


namespace MyConsole
{
  class Program
  {
    static void Main(string[] args)
    {
      //static class and static method
      var myFirstInt = MyFirstClass.GetMeSomeInt();

      //regular class and method
      var msc = new MySecondClass();


      var mySecondInt = msc.GetMeSomeInt();

      var result = MyNewMethod(64);
   }

      /*private method only accessible inside the Program class
     static, because the calling method is also static 
      int is the return typeof, meaning I'm going to get an integer back
        MyNewMetod is the method name
          param is an integer parameter
            this divides param value by 32 and returns the result*/
      
      private static int MyNewMethod(int param)
      {
        return param / 32;
    }

  }
}
